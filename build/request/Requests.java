package com.aqa.course.api.models.request;

import com.aqa.course.api.models.Customer;
import com.aqa.course.api.models.Specifications;
import com.google.gson.Gson;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

/**
 * Requests contain base API request without any verification.
 *
 * @author alexpshe
 * @version 1.0
 */
public class Requests {
    private final Specifications specifications;
    private final Gson gson;

    public Requests() {
        this.specifications = new Specifications();
        this.gson = new Gson();
    }

    public Response createCustomer(Customer customer) {
        return given()
                .spec(specifications.baseRequestSpec())
                .body(gson.toJson(customer))
                .post("/customer");
    }

    public Response getCustomer(String customerId) {
        return given()
                .spec(specifications.baseRequestSpec())
                .get("/customer/" + customerId);
    }

    public Response deleteCustomer(String customerId) {
        return given()
                .spec(specifications.baseRequestSpec())
                .delete("/customer/" + customerId);
    }
}

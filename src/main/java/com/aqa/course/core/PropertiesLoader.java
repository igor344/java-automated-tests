package com.aqa.course.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesLoader {
    private static final String CONFIG_PROPERTIES = "config.properties";
    private Properties properties;

    public String getProperty(String key) {            //Здесь мы получаем наш проперти по ключику. В данном случае это будет apiURL
        if (properties == null) {
            properties = loadProp();
        }
        return properties.getProperty(key);
    }
    private Properties loadProp() {
        File file = new File(PropertiesLoader.class.getClassLoader().getResource(CONFIG_PROPERTIES).getFile());
        properties = new Properties();

        try {
            properties.load(new FileInputStream(file));  //Здесь мы выкачиваем наши проперти
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return properties;
    }
}

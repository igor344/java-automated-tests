package com.aqa.course.core.config;

import com.aqa.course.core.PropertiesLoader;

public class ConfigOrder {
    private String apiUrlOrder;

    public ConfigOrder() {
        // if gradle task has apiUrl parameter Config.apiUrl, else apiUrl from config.properties
        this.apiUrlOrder = System.getProperty("apiUrlOrder") != null ?
                System.getProperty("apiUrlOrder") : new PropertiesLoader().getProperty("apiUrlOrder");
    }
    //./gradlew test -PapiUrl=https://..

    public String getApiUrlOrder() {
        return apiUrlOrder;
    }
}

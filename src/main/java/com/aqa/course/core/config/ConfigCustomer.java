package com.aqa.course.core.config;

import com.aqa.course.core.PropertiesLoader;

public class ConfigCustomer {
    private String apiUrlCustomer;

    public ConfigCustomer() {
        // if gradle task has apiUrl parameter Config.apiUrl, else apiUrl from config.properties
        this.apiUrlCustomer = System.getProperty("apiUrlCustomer") != null ?
                System.getProperty("apiUrlCustomer") : new PropertiesLoader().getProperty("apiUrlCustomer");
    }
    //./gradlew test -PapiUrl=https://..

    public String getApiUrlCustomer() {
        return apiUrlCustomer;
    }
}

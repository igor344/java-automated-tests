package com.aqa.course.ui.elements;

import com.aqa.course.ui.Constants;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selenide.element;

public class SubscriptionForm {
    private static final String SUBSCRIPTION_TITLE = "auth-placeholder__title ui-sb-h3";
    private static final String START_INSERT = "ui-sb-input__label ui-sb-16r ng-tns-c85-2 ng-star-inserted";
    private static final String INPUT = "//*[@id=\"ui-sb-input-element-0\"]";
    private static final String BACKGROUND = "/html/body/app-root/div/app-auth/div/div[2]/div/div[1]/app-auth-login/app-auth-card/div[2]";
    private static final String ERROR_MESSAGE = "ng-tns-c85-2 ui-sb-11r ng-star-inserted";

    public SelenideElement getTitle() {
        return element(byAttribute(Constants.CLASS, SUBSCRIPTION_TITLE)); // ������ ��� getter. �� ���� ������ ������� ��� ����� ���������� ������ "this"
    }

    public SubscriptionForm insertEmail(String email) {
        SelenideElement startInsert = element(byAttribute(Constants.CLASS, START_INSERT));
        startInsert.click();
        SelenideElement input = element(By.xpath(INPUT));
        input.setValue(email);
        return this;
    }

    public SelenideElement getErrorMessage() {
        SelenideElement background = element(By.xpath(BACKGROUND));
        background.click();
        return element(byAttribute(Constants.CLASS, ERROR_MESSAGE)); //��� ��� �������� �������� ����� ��� ������ ������� � ������� �� ������� ��� � ��������� ����� Constants
    }
}

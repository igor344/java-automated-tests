package com.aqa.course.ui.models;

public enum Theme {
    LIGHT("light"),
    DARK("dark");

    private final String attributeValue;

    Theme(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getAttributeValue() {
        return attributeValue;
    }
}

package com.aqa.course.ui.pages;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byAttribute;

public class NotelistPage {
    public NotelistPage open() {
        Selenide.open("https://notelist.ru/");
        return this;
    }
    public SelenideElement getTitle() {
        return Selenide.element(byAttribute("class", "lead"));
    }
    public SelenideElement getSubtitle() {
        return Selenide.element(byAttribute("class", "tagline"));
    }
    public NotelistPage insertNewNote(String text) {
        SelenideElement noteEditable = Selenide.element(byAttribute("class", "note-editable"));
        noteEditable.clear();
        noteEditable.setValue(text);
        return this;
    }
    public NotelistPage clickSave() {
        SelenideElement saveButton = Selenide.element(byAttribute("class", "duble_but_save_note"));
        saveButton.click();
        return this;
    }
    public SelenideElement getSuccessMessage() {
        return Selenide.element(byAttribute("class", "toaster toaster-right-bottom toasting"));
    }
}

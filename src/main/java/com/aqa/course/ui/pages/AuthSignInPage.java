package com.aqa.course.ui.pages;

import com.aqa.course.ui.elements.SubscriptionForm;
import com.codeborne.selenide.Selenide;

public class AuthSignInPage extends Page {
    private final SubscriptionForm subscriptionForm;

    public AuthSignInPage() {
        this.subscriptionForm = new SubscriptionForm();
    }

    public AuthSignInPage open() {
        Selenide.open("https://go.skillbox.ru/auth/sign-in");
        return this;
    }

    public SubscriptionForm getSubscriptionForm() {
        return subscriptionForm;
    }
}

package com.aqa.course.ui.pages;

import com.aqa.course.ui.Constants;
import com.aqa.course.ui.models.Theme;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selenide.element;

public class Page {
    public SelenideElement getTheme(Theme theme) {
        return element(byAttribute("data-theme", theme.getAttributeValue())); // ������ ��� getter. �� ���� ������ ������� ��� ����� ���������� ������ "this"
    }

    public void checkCurrentTheme(Theme theme) {
        getTheme(theme).shouldBe(Condition.exist);  // ������ ��� assertion. �� ���� ������ ������� ��� ����� ���������� ������ "this"
    }

    public Page changeTheme() {
        SelenideElement viewBox = element("svg[viewBox='0 0 100 70']");
        viewBox.click();

        SelenideElement privateSwitchBase = element(byAttribute(Constants.CLASS, "PrivateSwitchBase-input MuiSwitch-input css-1m9pwf3"));
        privateSwitchBase.click();
        return this;
    }
}

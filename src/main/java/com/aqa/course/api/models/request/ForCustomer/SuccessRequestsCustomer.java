package com.aqa.course.api.models.request.ForCustomer;

import com.aqa.course.api.models.Customer;
import com.aqa.course.api.models.data.Storage;
import org.apache.http.HttpStatus;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class SuccessRequestsCustomer {
    private RequestsCustomer request; // Это поле мы его заранее создаём и в рамках конструктора инициализируем новым объектом Request

    public SuccessRequestsCustomer() {
        this.request = new RequestsCustomer();
    }

    public void createCustomer(Customer customer) {
        request.createCustomer(customer)
                .then()
                .assertThat().statusCode(HttpStatus.SC_CREATED)
                .body(equalTo("Customer stored correctly"));

        Storage.getInstance().addId(customer.getId());
    }

    public Customer getCustomer(String customerId) {
        return request.getCustomer(customerId)
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK)
                .extract().body().as(Customer.class);      //extract вытаскивает сам запрос
    }

    public void deleteCustomer(String customerId) {
        request.deleteCustomer(customerId)
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK)
                .body(equalTo("Customer remove correctly"));
    }

    public void updateCustomer(Customer customer) {
        request.updateCustomer(customer)
                .then()
                .assertThat().statusCode(HttpStatus.SC_CREATED)
                .body(equalTo("Customer updated correctly"));
    }
}

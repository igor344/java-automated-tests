package com.aqa.course.api.models.data;

import com.aqa.course.api.models.Customer;

import java.util.ArrayList;
import java.util.List;

public class Storage {
    private static Storage instance;
    private List<String> ids;

    private Storage() {
        this.ids = new ArrayList<>();
    }

    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }

    public void addId(String id) {
         ids.add(id);
    }

    public List<String> getId() {
          return ids;
    }
}

package com.aqa.course.api.models.data;

import com.aqa.course.api.models.Customer;

public class Generator {
    private final RandomData randomData;

    public Generator() {
        this.randomData = new RandomData();
    }

    public Customer getCustomer() {
        return new Customer(randomData.getId(), randomData.getName(), randomData.getName(), randomData.getEmail());
    }
}

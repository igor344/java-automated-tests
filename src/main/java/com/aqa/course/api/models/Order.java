package com.aqa.course.api.models;

import java.util.HashMap;
import java.util.Objects;

public class Order {
    private String number;
    private HashMap<String, String> contents = new HashMap<>();

    public Order(String number, String item, String amount, String price) {
        this.number = number;
        contents.put("item", item);
        contents.put("amount", amount);
        contents.put("price", price);
    }

    public String getNumber() {
        return number;
    }

    public HashMap<String, String> getContents() {
        return contents;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setContents(HashMap<String, String> contents) {
        this.contents = contents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(number, order.number) && Objects.equals(contents, order.contents);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, contents);
    }
}

package com.aqa.course.api.models.data;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Random;

public class RandomData {
    private static final int DEFAULT_STRING_LENGTH = 10;
    private static final int DEFAULT_NAME_LENGTH = 7;
    public static final int DEFAULT_NUMBER_BOUND = 3;

    public String getId() {
        return RandomStringUtils.randomNumeric(DEFAULT_NUMBER_BOUND);
    }

    public String getString() {
        return RandomStringUtils.random(DEFAULT_STRING_LENGTH, true, false);
    }

    public String getName() {
        return RandomStringUtils.random(DEFAULT_NAME_LENGTH, true, false);
    }

    public String getEmail() {
        return getString() + "@gmail.com";
    }
}

package com.aqa.course.api.models.request.ForOrder;

import com.aqa.course.api.models.Order;
import com.aqa.course.api.models.Specifications;
import com.google.gson.Gson;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class RequestsOrder {
    Specifications specification;
    Gson gson;

    public RequestsOrder() {
        this.specification = new Specifications();
        this.gson = new Gson();
    }

    public Response createOrder(Order order) {
        return given()
                .spec(specification.baseRequestTypeForOrder())
                .body(gson.toJson(order))
                .post("/order");
    }

    public Response getOrder(String number) {
        return given()
                .spec(specification.baseRequestTypeForOrder())
                .get("/order/" + number);
    }

    public Response deleteOrder(String number) {
        return given()
                .spec(specification.baseRequestTypeForOrder())
                .delete("/delete/" + number);
    }

    public Response updateOrder(Order order) {
        return given()
                .spec(specification.baseRequestTypeForOrder())
                .body(gson.toJson(order))
                .put("/order/" + order.getNumber());
    }
}

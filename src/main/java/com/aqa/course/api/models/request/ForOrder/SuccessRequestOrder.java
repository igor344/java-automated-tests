package com.aqa.course.api.models.request.ForOrder;

import com.aqa.course.api.models.Order;
import org.apache.http.HttpStatus;

import static org.hamcrest.Matchers.equalTo;

public class SuccessRequestOrder {
    private RequestsOrder requestsOrder;

    public SuccessRequestOrder() {
        requestsOrder = new RequestsOrder();
    }

    public void createOrder(Order order) {
        requestsOrder.createOrder(order)
                .then()
                .assertThat().statusCode(HttpStatus.SC_CREATED)
                .body(equalTo("Order stored correctly"));
    }

    public Order getOrder(String number) {
        return requestsOrder.getOrder(number)
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK)
                .extract().body().as(Order.class);
    }

    public void deleteOrder(String number) {
        requestsOrder.deleteOrder(number)
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK)
                .body(equalTo("Order remove correctly"));
    }

    public void updateOrder(Order order) {
        requestsOrder.updateOrder(order)
                .then()
                .assertThat().statusCode(HttpStatus.SC_CREATED)
                .body(equalTo("Order update correctly"));
    }
}

package com.aqa.course.api.models.request.ForCustomer;

import com.aqa.course.api.models.Customer;
import com.aqa.course.api.models.Specifications;
import com.google.gson.Gson;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class RequestsCustomer {
    private Specifications specifications;
    private Gson gson;

    public RequestsCustomer() {
        this.specifications = new Specifications();
        this.gson = new Gson();
    }
    public Response createCustomer(Customer customer) {
        return given()
                .spec(specifications.baseRequestTypeForCustomer())
                .body(gson.toJson(customer))
                .post("/customer");
    }
    public Response getCustomer(String customerId) {
        return given()
                .spec(specifications.baseRequestTypeForCustomer())
                .get("/customer/" + customerId);
    }

    public Response deleteCustomer(String customerId) {
        return given()
                .spec(specifications.baseRequestTypeForCustomer())
                .delete("/customer" + customerId);
    }

    public Response updateCustomer(Customer customer) {
        return given()
                .spec(specifications.baseRequestTypeForCustomer())
                .body(gson.toJson(customer))
                .put("/customer/" + customer.getId());
    }
}

package com.aqa.course.api.models;

import com.aqa.course.core.config.ConfigCustomer;
import com.aqa.course.core.config.ConfigOrder;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

/**
 * Specifications содержит некую преднастройку запроса и ответа
 *
 * @author alexpshe
 * @version 1.0
 */
public class Specifications {
    public RequestSpecification baseRequestTypeForCustomer() {
         RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

         requestSpecBuilder.setContentType(ContentType.JSON);
         requestSpecBuilder.setBaseUri(new ConfigCustomer().getApiUrlCustomer());
         return requestSpecBuilder.build();
    }

    public RequestSpecification baseRequestTypeForOrder() {
        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

        requestSpecBuilder.setContentType(ContentType.JSON);
        requestSpecBuilder.setBaseUri(new ConfigOrder().getApiUrlOrder());
        return requestSpecBuilder.build();
    }
}

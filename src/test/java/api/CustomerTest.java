package api;

import com.aqa.course.api.models.Customer;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class CustomerTest extends BaseApiTest {

    @ParameterizedTest
    @DisplayName("Create customer")
    @MethodSource("customers")
    @Tag("Positive")
    public void createCustomerTest(Customer expectedCustomer) {
        successRequestsCustomer.createCustomer(expectedCustomer);
        Customer actualCustomer = successRequestsCustomer.getCustomer(expectedCustomer.getId());

        softly.assertThat(actualCustomer).isEqualTo(expectedCustomer);
    }

    private static Stream<Arguments> customers() {
        return Stream.of(
                Arguments.of(new Customer("100", "Jane", "Smith", "jane.smith@company.com")), //тест кейс 1
                Arguments.of(new Customer("101", "Kate", "Jone", "kate.smith@company.com")) //тест кейс 2
        );
    }

    @Test
    @DisplayName("Create customer and comparing by fields")
    @Tag("Positive")
    public void createCustomerAssertByBodeParametersTest() {
        Customer customer = new Customer("100", "Jane",
                "Smith", "jane.smith@company.com");

        requestsCustomer.createCustomer(customer)
                .then()
                .assertThat().statusCode(HttpStatus.SC_CREATED);

        requestsCustomer.getCustomer(customer.getId())
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK)
                .body("id", is("100"))
                .body("firstName", is("Jane"))
                .body("lastName", is("Smith"))
                .body("email", is("jane.smith@company.com"));
    }

    @Test
    @DisplayName("Create customer with existing id")
    @Tag("Negative")
    public void createUserWithExistingIdTest() {
        Customer customer = generator.getCustomer();

        successRequestsCustomer.createCustomer(customer);

        Customer newCustomer = generator.getCustomer();
        newCustomer.setId(customer.getId());

        requestsCustomer.createCustomer(newCustomer)
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body(equalTo("Customer with id " + customer.getId() + " already exists"));
    }

    @Test
    @DisplayName("Get customer")
    @Tag("Positive")
    public void getCustomerTest() {
        Customer customer = generator.getCustomer();

        successRequestsCustomer.createCustomer(customer);
        Customer actualCustomer = successRequestsCustomer.getCustomer(customer.getId());
        softly.assertThat(actualCustomer).isEqualTo(customer);
    }

    @Test
    @DisplayName("Get customer with a non-existing id")
    @Tag("Negative")
    public void getCustomerWithNonExistingIdTest() {
        Customer customer = generator.getCustomer();
        String generateId = randomData.getId();         //Генерируем новый id по которому будем искать несуществующего customer

        successRequestsCustomer.createCustomer(customer);
        requestsCustomer.getCustomer(generateId)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body(equalTo("Customer with id " + generateId + " non-exist"));
    }

    @Test
    @DisplayName("Update customer")
    @Tag("Positive")
    public void updateCustomerTest() {
        // Generation new customer
        Customer customer = generator.getCustomer();

        // Creation of generated customer
        successRequestsCustomer.createCustomer(customer);

        // Creation
        Customer updatedCustomer = generator.getCustomer();
        updatedCustomer.setId(customer.getId());

        //Updating of existing customer
        successRequestsCustomer.updateCustomer(updatedCustomer);

        Customer actualCustomer = successRequestsCustomer.getCustomer(customer.getId());

        // Assert that customer is updated
        softly.assertThat(actualCustomer).isEqualTo(updatedCustomer);
    }

    @Test
    @DisplayName("Update customer with a non-existing id")
    @Tag("Negative")
    public void updateCustomerWithNonExistingIdTest() {
        Customer customer = generator.getCustomer();
        requestsCustomer.updateCustomer(customer)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body(equalTo("Customer with id " + customer.getId() + " non-exist"));
    }

    @Test
    @DisplayName("Delete customer")
    @Tag("Positive")
    public void deleteCustomerTest() {
        Customer customer = generator.getCustomer();

        successRequestsCustomer.createCustomer(customer);
        successRequestsCustomer.deleteCustomer(customer.getId());

        requestsCustomer.getCustomer(customer.getId())
                .then()
                .assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }


    @Disabled
    @Test
    public void googleTest() {
        given()                                                        //то что нам Дано
                .get("http://google.com")
                .then()
                .assertThat().statusCode(HttpStatus.SC_OK);
    }
}

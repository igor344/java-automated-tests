package api;

import com.aqa.course.Calculator;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Homework.CalculatorTest includes tests for Calculator class.
 *
 * @author levin
 * @version 1.0
 */
public class CalculatorTest {
    Calculator calculator;
    SoftAssertions softly;

    @BeforeEach
    public void setup() {
        this.calculator = new Calculator();
        this.softly = new SoftAssertions();
    }

    @AfterEach
    public void assertAll() {
        softly.assertAll();
    }

    @DisplayName("Base positive test for sum")
    @ParameterizedTest
    @CsvSource({
            "2, 3, 5",
            "-10, -15, -25"
    })
    public void sumTest(int a, int b, int expectedResult) {

        int actualResult = calculator.sum(a, b);
        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Disabled
    @DisplayName("Base positive test for sumOfCollection")
    @ParameterizedTest                                                                 //Этот тест падет потому что в калькуляторе используется переменная identity,
    @MethodSource("provideIntegerForSumOfCollectionTest")                              //которая увеличиает сумму всех элементов коллекции на один. Скорее всего это ошибка кода.
    public void sumOfCollectionTest(List<Integer> numbers, long expectedResult) {
        long actualResult = calculator.sum(numbers);
        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }
    private static Stream<Arguments> provideIntegerForSumOfCollectionTest() {
        return Stream.of(
                Arguments.of(Arrays.asList(3, 2, 4, 1), 10),
                Arguments.of(Arrays.asList(-10, 2, 3), -5)
        );
    }

    @DisplayName("Base positive test for multiply")
    @ParameterizedTest
    @MethodSource("provideIntegerForMultiplyOfCollectionTest")
    public void multiplyOfCollectionTest(List<Integer> numbers, long expectedResult) {
          long actualResult = calculator.multiple(numbers);
          softly.assertThat(actualResult).isEqualTo(expectedResult);
    }
    private static Stream<Arguments> provideIntegerForMultiplyOfCollectionTest() {
        return Stream.of(
                Arguments.of(Arrays.asList(2, 5, 2), 20),
                Arguments.of(Arrays.asList(-2, 4), -8)
        );
    }

    @DisplayName("Base positive test for divide")
    @ParameterizedTest
    @CsvSource({
            "30, 5, 6",
            "-100, 10, -10"
    })
    public void divideTest(int divider, int divisor, int expectedResult) {
        double actualResult = calculator.divide(divider, divisor);
        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Disabled                                                              //Метод  возведения в степень в калькуляторе написан с двумя ошибками.
    @DisplayName("Base positive test for pow")                             //Скипаются 2 степени и первое умножение должно быть не на 1, а на само себя
    @ParameterizedTest
    @CsvSource({
            "2, 3, 8",
            "-3, 2, 9"
    })
    public void powTest(int base, int power, long expectedResult) {
        long actualResult = calculator.pow(base, power);
        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }
}

package api;

import com.aqa.course.api.models.data.Generator;
import com.aqa.course.api.models.data.RandomData;
import com.aqa.course.api.models.data.Storage;
import com.aqa.course.api.models.request.ForCustomer.SuccessRequestsCustomer;
import com.aqa.course.api.models.request.ForCustomer.RequestsCustomer;
import com.aqa.course.api.models.request.ForOrder.RequestsOrder;
import com.aqa.course.api.models.request.ForOrder.SuccessRequestOrder;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.*;

public class BaseApiTest {
    protected SoftAssertions softly;
    protected RequestsCustomer requestsCustomer;
    protected RequestsOrder requestsOrder;

    protected SuccessRequestsCustomer successRequestsCustomer;
    protected SuccessRequestOrder successRequestOrder;
    protected RandomData randomData;
    protected Generator generator;

    @BeforeAll
    public static void restAssured() {
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }

    @BeforeEach
    public void setupTestForOrder() {
        requestsOrder = new RequestsOrder();
        successRequestOrder = new SuccessRequestOrder();
        softly = new SoftAssertions();
    }



    /**
     * Перед запуском тестов на проверку создания Customer, проставить анотации BeforeEach и AfterEach у методов setupTest и cleanData.
     */
    @Tag("BeforeEach for Customer")
    public void setupTestForCustomer() {
        requestsCustomer = new RequestsCustomer();
        successRequestsCustomer = new SuccessRequestsCustomer();
        softly = new SoftAssertions();
        randomData = new RandomData();
        generator = new Generator();
    }

    @Tag("AfterEach for Customer")
    public void cleanData() {
        Storage.getInstance().getId().forEach(customerId -> requestsCustomer.deleteCustomer(customerId));
    }
}

package api;

import com.aqa.course.api.models.Order;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import static org.hamcrest.Matchers.equalTo;

public class OrderTest extends BaseApiTest {

    @Test
    @DisplayName("Create order")
    @Tag("Positive")
    public void createOrderTest() {
        Order expectedOrder = new Order("2020-04-06-10", "Ham Sandwich", "2", "5.5");

        successRequestOrder.createOrder(expectedOrder);
        Order actualOrder = successRequestOrder.getOrder(expectedOrder.getNumber());

        softly.assertThat(actualOrder).isEqualTo(expectedOrder);
    }

    @Test
    @DisplayName("Create order with existing number")
    @Tag("Negative")
    public void createOrderWithExistingNumber() {
        Order order = new Order("2020-04-06-10", "Ham Sandwich", "2", "5.5");
        successRequestOrder.createOrder(order);

        Order newOrder = new Order( "2020-04-06-10","Water", "1", "1.5");
        requestsOrder.createOrder(newOrder)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body(equalTo("Order with number " + order.getNumber() + " already exists"));
    }

    @Test
    @DisplayName("Get order")
    @Tag("Positive")
    public void getOrderTest() {
    Order expectedOrder = new Order("2020-04-06-10", "Ham Sandwich", "2", "5.5");

    successRequestOrder.createOrder(expectedOrder);
    Order actualOrder = successRequestOrder.getOrder(expectedOrder.getNumber());
    softly.assertThat(actualOrder).isEqualTo(expectedOrder);
    }

    @Test
    @DisplayName("Delete order")
    @Tag("Positive")
    public void deleteOrder() {
        Order expectedOrder = new Order("2020-04-06-10", "Ham Sandwich", "2", "5.5");

        successRequestOrder.createOrder(expectedOrder);
        successRequestOrder.deleteOrder(expectedOrder.getNumber());

        requestsOrder.getOrder(expectedOrder.getNumber())
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test
    @DisplayName("Update order")
    @Tag("Positive")
    public void updateOrder() {
        Order order = new Order("2020-04-06-10", "Ham Sandwich", "2", "5.5");

        successRequestOrder.createOrder(order);
        Order updateOrder = new Order( "2020-04-06-10","Water", "1", "1.5");
        successRequestOrder.updateOrder(updateOrder);

        Order actualOrder = successRequestOrder.getOrder(updateOrder.getNumber());
        softly.assertThat(actualOrder).isEqualTo(updateOrder);
    }
}

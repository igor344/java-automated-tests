package Draft;

import com.aqa.course.Calculator;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class CalculatorMyPracticeTest {
    private Calculator calculator;
    private SoftAssertions softly;

    //метод выполняемый перед всеми тестами
    @BeforeAll
    public static void setupBeforeAllTest(){
    }

    // метод выполняемый перед каждым тестом
    @BeforeEach
    public void setup() {
        this.calculator = new Calculator();
        this.softly = new SoftAssertions();
    }

    @AfterEach
    public void assertAll() {
        this.softly.assertAll(); //тут мы проверяем был кто-то их ассершинов не выполнен
    }

    @DisplayName("Base positive test for sum")
    //@RepeatedTest(0)           //Сколько раз нужно повторить тест
    @Tag("positive")
    @ParameterizedTest
    //@ValueSource(ints = {1, -10})
    @CsvSource({
            "1, 2, 3",
            "-10, -20, -30"
    })
    public void sumTest(int firstValue, int secondValue, int expectedResult) {

        //получаем фактический результат
        int actualResult = calculator.sum(firstValue, secondValue);

        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Test
    public void sumNegativeValueTest() {
        //начальные данные
        int firstValue = -10;
        int secondValue = -20;

        //ожидаемый результат
        int expectedResult = -30;

        //получаем фактический результат
        int actualResult = calculator.sum(firstValue, secondValue);

        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/test-data.csv")
    public void divideTest(int firstValue, int secondValue, int expectedResult) {
        double actualResult = calculator.divide(firstValue, secondValue);

        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }


    private static Stream<Arguments> testDataForSumOfCollectionValues() {
         return Stream.of(
                 Arguments.of(Arrays.asList(1, 3, 4), 8),
                 Arguments.of(Arrays.asList(-1, 0, 1), 0)
         );
    }

    @Disabled
    @ParameterizedTest
    @MethodSource("testDataForSumOfCollectionValues")
    public void sumOfCollectionValuesTest(List<Integer> numbers, long expectedResult) {
        long actualResult = calculator.sum(numbers);

        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }
    @Test
    @Disabled
    @Tag("broken")
    public void brokenTest() {}
}

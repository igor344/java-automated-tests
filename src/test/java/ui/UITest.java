package ui;

import com.aqa.course.ui.models.Theme;
import com.aqa.course.ui.pages.AuthSignInPage;
import com.aqa.course.ui.pages.PetrtcoiPage;
import com.codeborne.selenide.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selenide.element;

/**
 * UITest contains base UI tests with Selenide without Page Object pattern
 *
 * @author levin
 * @version 1.0
 */

public class UITest {
    private PetrtcoiPage petrtcoiPage;
    private AuthSignInPage authSignInPage;

    @BeforeAll
    public static void setup() {
        Configuration.browser = "CHROME";
//        Configuration.baseUrl = "https://petrtcoi.com/";
//        Configuration.remote = "http://188.243.214.3:4444/wd/hub/";        //Настройка запуска тестов на удалённом браузере в селеноиде(потом посмотреть видео более подробно как это делается. Данный адересс не актуален)
    }
    @BeforeEach
    public void createPage() {
        this.petrtcoiPage = new PetrtcoiPage();
        this.authSignInPage = new AuthSignInPage();
    }


    @Test
    public void webElementSelectorsTest() {
        Selenide.open("https://petrtcoi.com/");
        SelenideElement elementByXpath = element(By.xpath("//*[@id=\"__next\"]/header/button/div/svg/rect[2]"));
        SelenideElement elementByCSS = element("header[data-testid='layout-header']");
        SelenideElement elementByDataAttribute = element(byAttribute("data-testid", "layout-header"));
    }

    @Test
    public void defaultThemeIsDark() {
//        Selenide.open("https://petrtcoi.com/");
//        SelenideElement currentTheme = element(byAttribute("data-theme", "dark"));
//        currentTheme.shouldBe(Condition.exist);

        petrtcoiPage
                .open()
                .checkCurrentTheme(Theme.DARK);
    }

    @Test
    public void themeSwitcherTest() {
//        Selenide.open("https://petrtcoi.com/");
//
//        SelenideElement viewBox = element("svg[viewBox='0 0 100 70']");
//        viewBox.click();
//
//        SelenideElement privateSwitchBase = element(byAttribute("class", "PrivateSwitchBase-input MuiSwitch-input css-1m9pwf3"));
//        privateSwitchBase.click();
//
//        SelenideElement currentTheme = element(byAttribute("data-theme", "light"));
//        currentTheme.shouldBe(Condition.exist);
//        System.out.println();

        petrtcoiPage
                .open()
                .changeTheme()
                .checkCurrentTheme(Theme.LIGHT);
    }

    @Test
    public void subscriptionFormTest() {
//        Selenide.open("https://go.skillbox.ru/auth/sign-in");
//        SelenideElement title = element(byAttribute("class", "auth-placeholder__title ui-sb-h3"));
//        title.shouldHave(Condition.exactTextCaseSensitive("Узнавайте новое на Skillbox"));

        authSignInPage
                .open()
                .getSubscriptionForm()
                .getTitle()
                .shouldHave(Condition.exactTextCaseSensitive("Узнавайте новое на Skillbox"));
    }

    @Test
    public void emailValidationTest() {
//        Selenide.open("https://go.skillbox.ru/auth/sign-in");
//
//        String invalidEmail = "invalidEmail";
//        SelenideElement startInsert = element(byAttribute("class", "ui-sb-input__label ui-sb-16r ng-tns-c85-2 ng-star-inserted"));
//        startInsert.click();
//        SelenideElement input = element(By.xpath("//*[@id=\"ui-sb-input-element-0\"]"));
//        input.setValue(invalidEmail);
//
//        SelenideElement background = element(By.xpath("/html/body/app-root/div/app-auth/div/div[2]/div/div[1]/app-auth-login/app-auth-card/div[2]"));
//        background.click();
//        SelenideElement errorMessage = element(byAttribute("class", "ng-tns-c85-2 ui-sb-11r ng-star-inserted"));
//
//        errorMessage.shouldHave(Condition.exactTextCaseSensitive("Неверный формат e-mail")); //Проверяем, что текст в html пришёл правилно
//        errorMessage.shouldBe(Condition.appear);                                             //Проверяем, что текст отобразился на экране

        authSignInPage
                .open()
                .getSubscriptionForm()
                .insertEmail("invalidEmail")
                .getErrorMessage()
                .shouldHave(Condition.exactTextCaseSensitive("Неверный формат e-mail"))
                .shouldBe(Condition.appear);
    }
}

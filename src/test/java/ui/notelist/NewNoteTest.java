package ui.notelist;

import com.aqa.course.ui.pages.NotelistPage;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selectors.byAttribute;

/**
 * Задача №2: Новая заметка; Задача №3: Page Object
 *
 * @author levin
 * @version 1.0
 */
public class NewNoteTest {
    private NotelistPage notelistPage;

    @BeforeAll
    public static void setup() {
//        Configuration.baseUrl = "https://notelist.ru/";
//        Configuration.timeout = 1000;
    }
    @BeforeEach
    public void createPage() {
        this.notelistPage = new NotelistPage();
    }

    @Test
    public void addNewNote() {
//        Selenide.open("");

//        //Очищаем поле и пишем новую заметку
//        SelenideElement noteEditable = Selenide.element(byAttribute("class", "note-editable"));
//        noteEditable.clear();
//        noteEditable.setValue("Checking adding a new note");

//        //Нажимаем кнопку сохранить
//        SelenideElement saveButton = Selenide.element(byAttribute("class", "duble_but_save_note"));
//        saveButton.click();

//        //Проверяем, что заметка успешно сохранена
//        SelenideElement successMessage = Selenide.element(byAttribute("class", "toaster toaster-right-bottom toasting"));
//        successMessage.shouldHave(Condition.exactTextCaseSensitive("Запись сохранена!"));
//        successMessage.shouldBe(Condition.appear);

        notelistPage
                .open()
                .insertNewNote("Checking adding a new note")
                .clickSave()
                .getSuccessMessage()
                .shouldHave(Condition.exactTextCaseSensitive("Запись сохранена!"))
                .shouldBe(Condition.appear);
    }
}

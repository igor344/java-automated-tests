package ui.notelist;

import com.aqa.course.ui.pages.NotelistPage;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selectors.byAttribute;

/**
 * Задача №1: Заголовки на сайте; Задача №3: Page Object
 *
 * @author levin
 * @version 1.0
 */
public class TitlesTest {
    private NotelistPage notelistPage;

    @BeforeAll
    public static void setup() {
//        Configuration.baseUrl = "https://notelist.ru/";
    }
    @BeforeEach
    public void createPage() {
        this.notelistPage = new NotelistPage();
    }

    @Test
    public void checkTitle() {
//        Selenide.open("");
//        SelenideElement title = Selenide.element(byAttribute("class", "lead"));
//        title.shouldHave(Condition.exactTextCaseSensitive("Блокнот Онлайн"));
//        title.shouldBe(Condition.appear);

        notelistPage
                .open()
                .getTitle()
                .shouldHave(Condition.exactTextCaseSensitive("Блокнот Онлайн"))
                .shouldBe(Condition.appear);
    }

    @Test
    public void checkSubtitle() {
//        Selenide.open("");
//        SelenideElement subtitle = Selenide.element(byAttribute("class", "tagline"));
//        subtitle.shouldHave(Condition.exactTextCaseSensitive("сохраняй заметки и получи быстрый доступ к ним в любое время!"));
//        subtitle.shouldBe(Condition.appear);

        notelistPage
                .open()
                .getSubtitle()
                .shouldHave(Condition.exactTextCaseSensitive("сохраняй заметки и получи быстрый доступ к ним в любое время!"))
                .shouldBe(Condition.appear);

    }

}
